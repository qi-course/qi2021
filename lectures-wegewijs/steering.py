#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from  matplotlib.colors import LinearSegmentedColormap



### Vectors

def M1non(c,l):
    s0 = np.sqrt(l); s1 = np.sqrt(1-s0**2)
    v0=np.cos(c); v1=np.sin(c)
    return np.transpose(np.array([[s0*v0,s1*v1],[-s0*v1,s1*v0]]))

def M1(c,l):
    s0 = np.sqrt(l); s1 = np.sqrt(1-s0**2)
    x = M1non(c,l)
    return x/np.linalg.norm(x,ord=2,axis=0)

def M2(c):
    v0=np.cos(c); v1=np.sin(c);
    return np.transpose(np.array([[v0,v1],[-v1,v0]]))



class Steering:
    # define red-green colormap
    colorlist = ["red","red","limegreen"]
    segmentlist = [0.,0.4, 1.]
    cmap=LinearSegmentedColormap.from_list('rg',
        list(zip(segmentlist,colorlist)), N=20)
    arrow_params = dict(
            head_width=0.05,
            head_length=0.05,
            length_includes_head=True
            )
    alice_arrow_params = dict(width=0.01, color='black', **arrow_params)
    bob_arrow_params = dict(width=0.01, color=(0.,0.,1.), **arrow_params)
    bob_mirror_arrow_params = dict(width=0.01, color=(0.2,0.2,1.,0.3), **arrow_params)
    marginal_arrow_params = dict(width=0.001, color='lightgray',linestyle='--', **arrow_params)

    def __init__(self, resolution=200):
        self.fig = plt.figure(figsize=plt.figaspect(0.5))
        self.fig.subplots_adjust(left=0.15, bottom=0.25) 

        self.ax1 = self.fig.add_subplot(1, 2, 1)
        self.ax2 = self.fig.add_subplot(1, 2, 2)

        self.ax1.axis('equal')  # set the axes to the same scale
        self.ax2.axis('equal')
        self.ax1.plot(0, 0, 'ok') # plot a black point at the origin
        self.ax2.plot(0, 0, 'ok') 
        self.arrows_alice = []
        self.arrows_bob = []
        m = 0.15 # margin
        self.ax1.set_xlim([-1.0-m,1.0+m])
        self.ax1.set_ylim([-1.0-m,1.0+m])
        self.ax2.set_xlim([-1.0-m,1.0+m])
        self.ax2.set_ylim([-1.0-m,1.0+m])

        self.drag_pointer = 0

        # Hide axes
        self.ax1.xaxis.set_visible(False)
        self.ax1.yaxis.set_visible(False)
        self.ax2.xaxis.set_visible(False)
        self.ax2.yaxis.set_visible(False)

        self.ax1.set_title('Alice: Ensemble state vectors\n'
                      r'$\vert \psi_{Ab} \rangle = \langle b_B \vert \psi_{AB} \rangle / |\langle b_B \vert \psi_{AB} \rangle|$')
        self.ax2.set_title('Bob: Observation vectors\n'
                      r'$\vert b_{B} \rangle$',)
        self.ax1.arrow(-1,0,2,0,**self.marginal_arrow_params,zorder=-2)
        self.ax1.arrow(0,-1,0,2,**self.marginal_arrow_params,zorder=-2)
        self.ax2.arrow(-1,0,2,0,**self.marginal_arrow_params,zorder=-2)
        self.ax2.arrow(0,-1,0,2,**self.marginal_arrow_params,zorder=-2)

        self.ax1.text(0.85,-0.2, 'eigen-\n' r'vectors $\rho_A$',
                      horizontalalignment='center', verticalalignment='center',
                      color = 'grey')
        self.ax2.text(0.85,-0.2, 'eigen-\n' r'vectors $\rho_B$',
                      horizontalalignment='center', verticalalignment='center',
                      color = 'grey')
        self.ax2.text(-0.75,0.925, 'Grab vectors\n to rotate',
                      horizontalalignment='center', verticalalignment='center',
                      color = 'grey')

        self.angles = np.linspace(0, 2*np.pi, resolution)



        #### Slider ###
        # Define initial values.
        l_min = 1e-5   # avoid ugly drawings for l=0 ...
        c = 0*np.pi
        l = 0.5

        ax_slider_c =  plt.axes([0.3, 0.1, 0.55, 0.03])
        self.slider_c = Slider(
                ax_slider_c,
                'Eigenbasis $\\rho_B$: angle $\\theta$ $\\in$ [-$\\pi$, 3$\\pi$]',
                -1*np.pi,
                3*np.pi,
                valinit=c
                )
        ax_slider_l =  plt.axes([0.3, 0.05, 0.55, 0.03])
        self.slider_l = Slider(
                ax_slider_l,
                'Eigenvalue $\\rho_B$: $\lambda_0$ $\\in$ [0,1]',
                l_min,
                1-l_min,
                valinit=l
                )

        self.slider_c.on_changed(self.update)
        self.slider_l.on_changed(self.update)

        # Dragging the arrows requires handling mouse input events:
        self.fig.canvas.mpl_connect('button_press_event', self.on_press)
        self.fig.canvas.mpl_connect('button_release_event', self.on_release)
        self.fig.canvas.mpl_connect('motion_notify_event', self.drag)

    def on_press(self, event):
        'Mouse button press event: check if this should start dragging an arrow'
        if event.inaxes != self.ax2:
            return
        if self.arrows_bob[0].contains(event, 500)[0] or self.arrows_bob[1].contains(event, 500)[0]:
            vec1 = (event.xdata - self.arrows_bob[0].xy[0,0], event.ydata - self.arrows_bob[0].xy[0,1])
            vec2 = (event.xdata - self.arrows_bob[1].xy[0,0], event.ydata - self.arrows_bob[1].xy[0,1])
            if (vec1[0]**2 + vec1[1]**2 < vec2[0]**2 + vec2[1]**2):
                self.drag_pointer = 1
            else:
                self.drag_pointer = 2


    def on_release(self, event):
        'Stop dragging arrow'
        self.drag_pointer = 0

    def drag(self, event):
        '''Drag one of Bob's arrows'''
        if event.inaxes != self.ax2:
            return
        if self.drag_pointer == 1:
            self.slider_c.set_val(np.arctan2(event.ydata, event.xdata))
        elif self.drag_pointer == 2:
            self.slider_c.set_val(np.arctan2(-event.xdata, event.ydata))


    def run(self):
        'Start the animation.'
        self.update()
        plt.show()


    def steer(self, c, l):
        # Alice (left panel)
        for arr in self.arrows_alice:
            arr.remove()
        for arr in self.arrows_bob:
            arr.remove()
        self.arrows_alice.clear()
        self.arrows_bob.clear()
        x = np.sqrt(l)*np.cos(self.angles)
        y = np.sqrt(1-l)*np.sin(self.angles)
        try:
            self.ellipse.set_data(x, y)
        except AttributeError:
            self.ellipse, = self.ax1.plot(x, y, color='lightgray', zorder=-1)
        M = M2(c)
        for i in range(0, 2):
            self.arrows_alice.append(
                    self.ax1.arrow(0, 0, M[0,i], M[1,i], **self.bob_mirror_arrow_params)
                    )
        M = M1(c,l)
        for i in range(0, 2):
            self.arrows_alice.append(
                    self.ax1.arrow(0, 0, M[0,i], M[1,i], **self.alice_arrow_params)
                    )
        M = M1non(c,l)
        for i in range(0, 2):
            self.arrows_alice.append(
                    self.ax1.arrow(0, 0, M[0,i], M[1,i], **self.arrow_params, width=0.05,
                      color = self.cmap( np.sqrt(M[0,i]**2+M[1,i]**2) ) )
                    )
        # Bob (right panel)    
        M = M2(c)
        for i in range(0, 2):
            self.arrows_bob.append(
                    self.ax2.arrow(0, 0, M[0,i], M[1,i], **self.bob_arrow_params)
                    )


    def update(self, *args): 
        'Read values from the sliders and update the figure'
        c = self.slider_c.val
        l = self.slider_l.val
        self.steer(c, l)
        self.fig.canvas.draw_idle()



if __name__ == '__main__':
    Steering().run()
