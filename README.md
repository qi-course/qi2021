# Quantum Information course 2021
This is a collection of programming tasks and scripts used in the lecture on quantum information theory in summer semester 2021 at RWTH University.

If you want to improve the programs in this repository, feel free to open a merge request or an issue.

All files in this repository for which no other license is specified may be reused under the terms of the MIT license.
