#!/usr/bin/env python3
'''
Problem 2e of exercise 11: Quantum phase estimation of a T gate.
'''

#initialization
import matplotlib.pyplot as plt
import numpy as np
import math

# importing Qiskit
from qiskit import Aer, transpile, assemble
from qiskit import QuantumCircuit, ClassicalRegister, QuantumRegister

# import basic plot tools
from qiskit.visualization import plot_histogram

# you may use this function to execute the inverse fourier transform or write your own
def qft_dagger(qc, n):
    '''
    Perform inverse quantum fourier transform on the first n qubits of
    the quantum circuit qc.
    '''
    for qubit in range(n//2):
        qc.swap(qubit, n-qubit-1)
    for j in range(n):
        for m in range(j):
            qc.cp(-math.pi/float(2**(j-m)), m, j)
        qc.h(j)


def main():
    # (i) explain the following line: Why is the initialization done like this? Which qubits fulfill which function?
    qpe = QuantumCircuit(QuantumRegister(4), ClassicalRegister(3))

    # (ii) add the correct qubit initialization and control gates for quantum phase estimation 

    # (iii) add inverse qft and counting qubit measurements

    # run your circuit with this simulator configuration
    aer_sim = Aer.get_backend('aer_simulator')
    shots = 2048
    t_qpe = transpile(qpe, aer_sim)
    qobj = assemble(t_qpe, shots=shots)
    results = aer_sim.run(qobj).result()
    answer = results.get_counts()

    plot_histogram(answer)
    # (iv) what is the result and the resulting phase? what is the error? 


if __name__ == '__main__':
    main()
