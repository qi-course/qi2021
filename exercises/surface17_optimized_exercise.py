#!/usr/bin/env python3
"""
Surface-17 quantum error correction code, simulation of dephasing noise.

This montecarlo simulation generates phase flip errors, does one quantum error
correction cycle, and checks whether the noise has caused a logical Z error.
The order of the qubits and stabilizers follows Figure 4 on the exercise sheet.

Required packages:
    numpy
    matplotlib
    numba

Compared to surface17.py, this optimized version replaces some functions by
lookup tables. Other functions are made much faster using numba's just in time
compilation (jit).
"""
import numpy as np
from random import random
import matplotlib.pyplot as plt
from numba import jit, prange

# We need to define for logical operators and for stabilizers the set qubits on
# which they act.
# This can be done using binary representation of an integer, in which the last
# bit indicates the physical qubit D1, and so on. For example, a logical
# X operator that acts on qubits D1, D4, and D7 is defined as:
xL = 0b001001001

# X stabilizer generators in symplectic notation
# qubit:987654321
s1 =  0b000000110
s2 =  0b000011011
s3 =  0b110110000
s4 =  0b011000000
stabilizerGenerators = (s1, s2, s3, s4)

# Lookup table of number of bits:
# parity_table[x] = 1 if x has an odd number of ones, 0 otherwise
# This implies getCommutator(a, b) == parity_table[a & b]
parity_table = bytes(bin(x).count('1')%2 for x in range(1<<9))

@jit(nopython=True)
def getSyndrome(configuration):
    """
    Given a configuration of phase flip errors on the data qubits (defined by
    the binary representation of the integer "configuration"), compute the
    syndrome.
    Commutation/anticommutation with stabilizer n is marked as 0/1 in bit n
    of the syndrome. E.g. if an error anticommutes with stabilizers 1 and 3,
    the syndrome will be 0b0101.
    """
    syndrome = 0
    for i, s in enumerate(stabilizerGenerators):
        syndrome |= parity_table[s & configuration] << i
    return syndrome

# Lookup table of corrections:
# correction_table[syndrome] == getCorrection(syndrome)
correction_table = (
        # TASK: add the correct syndromes and corrections
        0b000000000, # syndrome = 0, correction = []
        0b000000100, # syndrome = 0b0001, correction = [3]
        0b000000001, # syndrome = 0b0010, correction = [1]
        0b000000010, # syndrome = 0b0011, correction = [2]
        0b100000000, # syndrome = 0b0100, correction = [9]
        0b100000100, # syndrome = 0b0101, correction = [3,9]
        ...        , # syndrome = 0b0110, correction = ...
        0b100000010, # syndrome = 0b0111, correction = [2,9]
        ...        , # syndrome = 0b1000, correction = ...
        ...        , # syndrome = 0b1001, correction = ...
        0b001000001, # syndrome = 0b1010, correction = [1,7]
        0b001000010, # syndrome = 0b1011, correction = [2,7]
        ...        , # syndrome = 0b1100, correction = ...
        0b010000100, # syndrome = 0b1101, correction = [3,8]
        0b010000001, # syndrome = 0b1110, correction = [1,8]
        0b010000010, # syndrome = 0b1111, correction = [2,8]
    )


@jit(nopython=True, cache=False, parallel=True)
def simulate(p, samples):
    """
    Simulate n samples with error probability p.
    Return logical failure rate.
    """
    failures = 0
    # sample a reasonable amount of times given the probability of errors
    for _ in prange(samples):

        # start out with clean state and randomly place Z-errors on data qubits
        configuration = 0
        for i in range(9):
            if random() < p:
                configuration |= 1<<i

        # TASK: add syndrome readout and apply correction operators according to look up table
        syndrome = ...
        correction = ...
        after_correction = configuration ^ correction

        # TASK: check if the resulting configuration has caused a logical Z error
        if ... :
            failures += 1

    # calculate logical failure rate from samples
    return failures / samples


def main():
    # set range of physical error rates to MC sample
    pRange = np.logspace(-3, 0, 21)

    # Run the simulation
    logical_failure_rate = [simulate(p, int(5000/p)) for p in pRange]

    # Plot the results
    plt.grid()
    plt.xlabel('$p$')
    plt.ylabel('$p_L$')
    plt.title('Pseudothreshold of Surface-17')
    plt.loglog(pRange, logical_failure_rate, '-x')
    plt.loglog(pRange, pRange, 'k:', alpha=0.5)
    plt.show()


if __name__ == '__main__':
    main()
